#!/usr/bin/python
import os, sqlite3, getopt, sys
from fuzzywuzzy import process, fuzz
import prettytable
from prettytable import PrettyTable
from helper import help, number_list

# MAIN CLASS
class MovieJournal(object):
# INIT 
    def __init__(self):
        super().__init__()
        self.norate = False
        self.mtitle = ""
        self.mrate = ""
        self.number = True
        self.editMode = False
        self.check_dir()
        self.get_arg()

# HANDLE COM ARGUMENTS
    def get_arg(self):
        try:
            opts, args = getopt.getopt(sys.argv[1:], "ahl::nm:r:d:e:s:o:S:N", ["help", "list", "movie", "rate", "delete", "edit", "norate", "add", "search", "output", "sort", "nonumber"])
            if(len(opts) == 0):
                help()
            for opt, arg in opts:
                if opt in ["-h", "--help"]:
                    help()
                elif opt in ["-l", "--list"]:
                    self.list_movies(arg)
                elif opt in ["-e", "--edit"]:
                    self.edit_movies(arg)
                elif opt in ["-r", "--rate"]:
                    self.mrate = arg
                elif opt in ["-m", "--movie"]:
                    self.mtitle = arg
                elif opt in ["-n", "--norate"]:
                    self.norate = True
                elif opt in ["-a", "--add"]:
                    self.add_movies()
                elif opt in ["-d", "--delete"]:
                    self.delete_movie(arg)
                elif opt in ["-S", "--search"]:
                    self.search_movie(arg)
                elif opt in ["-s", "--sort"]:
                    self.search_movie(arg)
                elif opt in ["-o", "--output"]:
                    self.output_file(arg)
                elif opt in ["-N", "--nonumber"]:
                    self.number = False
                else:
                    help()
        except getopt.GetoptError:
            help()

# OUTPUT FILE
    def output_file(self, arg):
        fname, ftype = (os.path.splitext(arg))
        ftype =ftype[1:]
        supported_docs = ["txt", "ini", "org", "html", "latex", "md"]
        if ftype in supported_docs:
            self.outputFile = arg
            if ftype == "org":
                self.outputOrg()
        else:
            print("Output format not supported")

# OUTPUT ORG DOC
    def outputOrg(self):
        movs = []
        rats = []
        movies = self.cursor.execute('SELECT * FROM movies').fetchall()
        for m in movies:
            movs.append(m[0])
            rats.append(m[1])
        with open(self.outputFile, 'w') as f:
            f.write('| TITLE | RATING |\n')
            f.write('|-------+--------|\n')
            for i in range(len(movs)):
                # f.write('{0}. {1}\n'.format(i+1, movs[i])) #
                f.write('| {0} | {1} |\n'.format(movs[i], rats[i]))
            f.write('|-------+--------|')

# SEARCH MOVIE IN DATABASE
    def search_movie(self, arg):
        movs = []
        movies = self.cursor.execute('SELECT * FROM movies').fetchall()
        for m in movies:
            movs.append(m[0])

        res = process.extract(arg, movs, limit=None)
        for i in range(len(res)):
            print("{0}. {1}".format(i+1, res[i][0]))

# ADD MOVIE TO DATABASE
    def add_movies(self):
        if(self.editMode):
            movs =[]
            noTitleUpdate = False
            noRateUpdate = True
            movies = self.cursor.execute('SELECT TITLE FROM movies').fetchall()
            for m, in movies:
                movs.append(m)
            if self.editTitle in movs:
                if(self.mrate != ""):
                    noRateUpdate = False

                if(self.mtitle == ""):
                    noTitleUpdate = True

                if noTitleUpdate and not noRateUpdate:
                    self.cursor.execute("UPDATE movies SET RATE = '{0}' WHERE TITLE = '{1}'".format(self.mrate, self.editTitle))
                elif not noTitleUpdate and noRateUpdate:
                    self.cursor.execute("UPDATE movies SET TITLE = '{0}' WHERE TITLE = '{1}'".format(self.mtitle, self.editTitle))
                elif not noTitleUpdate and not noRateUpdate:
                    self.cursor.execute("UPDATE movies SET TITLE = '{0}', RATE = '{1}' WHERE TITLE = '{2}'".format(self.mtitle, self.mrate, self.editTitle))
            else:
                res = process.extractBests(self.editTitle, movs)
                for r in range(len(res)):
                    print("{0}.'{1}'".format(r+1, res[r][0]))
                if(len(res) > 0):
                    num = input("ENTER THE NUMBER OF THE MOVIE TO DELETE:")
                    if(self.mrate != ""):
                        noRateUpdate = False

                    if(self.mtitle == ""):
                        noTitleUpdate = True
                    if noTitleUpdate and not noRateUpdate:
                        self.cursor.execute("UPDATE movies SET RATE = '{0}' WHERE TITLE = '{1}'".format(self.mrate, res[int(num)-1][0]))
                    elif not noTitleUpdate and noRateUpdate:
                        self.cursor.execute("UPDATE movies SET TITLE = '{0}' WHERE TITLE = '{1}'".format(self.mtitle, res[int(num)-1][0]))
                    elif not noTitleUpdate and not noRateUpdate:
                        self.cursor.execute("UPDATE movies SET TITLE = '{0}',RATE = '{1}' WHERE TITLE = '{2}'".format(self.mtitle, self.mrate, res[int(num)-1][0]))
            self.c.commit()
        else:
            self.cursor.execute('''
                CREATE TABLE IF NOT EXISTS movies
                (TITLE TEXT NOT NULL,
                RATE TEXT
                )
            ''')
            x = self.cursor.execute("SELECT TITLE FROM movies WHERE TITLE ='{0}' ".format(self.mtitle)).fetchall()
            if(len(x) > 0):
                print("Movie {0} has already been rated".format(self.mtitle))
                exit(0)

            if(self.mtitle == ""):
                print("Please give a title")
                exit(0)

            elif(self.mrate == ""):
                if not self.norate:
                    print("No rating found. If you dont wanna rate, pass the -n option")
                    exit(0)

            self.cursor.execute("INSERT INTO movies VALUES ('{0}', '{1}')".format(self.mtitle, self.mrate))
            print("Added {0}".format(self.mtitle))
        self.c.commit()

# EDIT ADDED MOVIES
    def edit_movies(self, title):
        self.editMode = True
        self.editTitle = title

# DELETE EXISTING MOVIES
    def delete_movie(self, title):
        movs =[]
        movies = self.cursor.execute('SELECT TITLE FROM movies').fetchall()
        for m, in movies:
            movs.append(m)
        if title in movs:
            self.cursor.execute("DELETE FROM movies WHERE TITLE = '{0}'".format(title))
            print("Deleted {0}".format(title))
        else:
            p = process.extract(title, movs)
            for m in range(len(p)):
                print("{0}. '{1}'".format(m+1, p[m][0]))
            if(len(p) > 0):
                num = input("ENTER THE NUMBER OF THE MOVIE TO DELETE:")
                print("Deleted {0}".format(p[int(num)-1][0]))
                self.cursor.execute("DELETE FROM movies WHERE TITLE = '{0}'".format(p[int(num)-1]))
        self.c.commit()
# LIST STORED MOVIES
    def list_movies(self, mode='ord github'):
        try:
            mode, style =mode.split(' ')
        except ValueError:
            mode, style = 'tab', 'github'
        x = self.cursor.execute('SELECT TITLE, RATE FROM movies ORDER BY CAST(RATE as Integers) DESC, TITLE').fetchall()
        if(len(x) == 0):
            print("No movies found")
            exit(0)
        x = number_list(x)
        if(mode == 'tab' or mode == 't'):
            t = PrettyTable()
            t.border = True
            t.vrules = prettytable.ALL
            t.hrules = prettytable.ALL
            t.junction_char = '|'
            t.field_names = ["No.", "Title", "Rating"]
            t.align["No."] = "l"
            t.align["Title"] = "c"
            t.align["Rate"] = "r"
            t.add_rows(x)

            print(t)

# CHECK IF CONFIG DIR EXISTS
    def check_dir(self):
        self.home = os.getenv('HOME')
        self.confdir = self.home + '/.config/moviejournal/'
        if not os.path.exists(self.confdir):
            os.mkdir(self.confdir)
        self.c = sqlite3.connect(self.confdir + 'mj.db')
        self.cursor = self.c.cursor()

if __name__ == "__main__":
    s = MovieJournal()
