#!/usr/bin/python
def help():
    print("\nCommand format:")
    print("moviejournal [OPTIONS] [ARGS]")
    print("OPTIONS:")
    print("-h, --help\tThis Help Menu")
    print("-m, --movie\tMovie title")
    print("-r, --rate\tRating")
    print("-n, --norate\tNo Rating Mode")
    print("-g, --genre\tMovie genre")
    print("-l, --list\tList added movies")
    print("-s, --search\tSearch for a stored movie")
    print("-d, --delete\tDelete movie")
    print("-e, --edit\tEdit existing movie")
    print("-a, --add\tAdd the passed movie details to the database")
    print("\nNOTE:\n1.Consider aliasing moviejournal command to something short like mj")
    print("2.The -a option is mandatory for all the commands, and is mandatory that it be the last option of the command")
    print("\nEXAMPLES:")
    print("To add '5' rating for the movie 'Saw I':")
    print("$ mj -m 'Saw I' -r 5 -a")
    print("\nTo edit the rating for the movie 'Saw I' to '8.5':")
    print("$ mj -e 'Saw I' -r 8.5 -a")
    print("\nTo edit the name 'Saw I' to 'Jeepers Creepers' with rating from '8.5' to '10':")
    print("$ mj -e 'Saw I' -m 'Jeepers Creepers' -r 10 -a")
    print("\nNow to edit the rating for 'Jeepers Creepers' to 8.5, you need not pass the entire movie name, moviejournal comes\ninbuilt with a fuzzy search (fuzzywuzzy), which helps if passed movie names dont match\ncompletely with the saved movies. So you can do:\n")
    print("$ mj -e 'Jeepe' -r 8.5 -a")
    print("\nWhich gives you a prompt to select the film that matches the passed name, something like:")
    print("\n1.Jeeps\n2.Jeepeee\n3.Jeepers Creepers\n4.Jeepers Creepers II\nEnter the valid choice to see the change.")
    print("\nTo delete an existing movie, simple pass the -d option with the name, again\nif the passed name matches some movie in the db, then it \ndeletes it, otherwise prompts you with the matching results:\n")
    print("$ mj -d 'Jeepers Creepers'")

def number_list(arr):
    b = arr
    for i in range(len(b)):
        b[i] = (str(i+1) + ".", ) + b[i] 
    return b

